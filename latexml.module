<?php

/**
 * A module that allows new input types such as LaTeX and sTeX
 */

/**
 * Implements hook_filter_info().
 */
function latexml_filter_info() {
  $filters = array();
  
  $filters['latex'] = array(
    'title' => t('LaTeX filter'),
    'description' => t('The text will be passed as LaTeX to LaTeXML daemon and then shown to the user'),
    'process callback' => '_latexml_filter_latex_process',
    'default settings' => array(
      'latexml_url' => 'http://latexml.mathweb.org/convert',
      'latexml_profile' => 'planetmath',
      'latexml_preamble' => '',
    ),
    'cache' => true,
    'settings callback' => '_latexml_filter_latex_settings',
    'tips callback' => '_latexml_filter_latex_tips',
  );
  return $filters;
}


/**
 * Settings callback for LaTeX filter
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @param unknown_type $filter
 * @param unknown_type $format
 * @param unknown_type $defaults
 */
function _latexml_filter_latex_settings($form, $form_state, $filter, $format, $defaults) {
  $settings['latexml_url'] = array(
    '#type' => 'textfield',
    '#title' => t('LaTeXML daemon URL'),
    '#default_value' => isset($filter->settings['latexml_url']) ? $filter->settings['latexml_url'] : $defaults['latexml_url'],
    '#description' => t('The URL of the LaTeXMl daemon.'),
  );
  $settings['latexml_profile'] = array(
    '#type' => 'textfield',
    '#title' => t('Profile to be used'),
    '#default_value' => isset($filter->settings['latexml_profile']) ? $filter->settings['latexml_profile'] : $defaults['latexml_profile'],
    '#description' => t('The profile to be used for the daemon.'),
  );
  $settings['latexml_preamble'] = array(
    '#type' => 'textarea',
    '#title' => t('Preamble to be used'),
    '#default_value' => isset($filter->settings['latexml_preamble']) ? $filter->settings['latexml_preamble'] : $defaults['latexml_preamble'],
    '#description' => t('The preamble to be used for the daemon.'),
  );
  $settings['latexml_reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset LaTeXML cache'),
    '#submit' => array('_latexml_filter_reset_cache'),
  );
  return $settings;
}


function _latexml_filter_reset_cache($form, &$form_state) {
  cache_clear_all(NULL, "latexml_cache");
}

function process_latexml($host, $text, $profile, $preamble) {
  $data = array();
  $data["profile"] = $profile;
  $data["tex"]=$text;
  if (isset($preamble) && strlen($preamble)>0) {
    $data["preamble"]="literal:".$preamble;
  }

  $data_str = http_build_query($data);

  $content = drupal_http_request($host, array("method"=>"post", "data" => $data_str, "headers"=>array('Content-Type' => 'application/x-www-form-urlencoded')));
  if ($content->code != 200) {
    drupal_set_message("LaTeXML returned an error ",$content->data);
    return "";
  } else {
    $res = json_decode($content->data);
    $res = trim($res->{'result'});
  }
  return $res;
}

/**
 * LaTeX filter process callback
 * @see latexml_filter_info
 *
 * @param unknown_type $text
 * @param unknown_type $filter
 * @param unknown_type $format
 */
function _latexml_filter_latex_process($text, $filter, $format, $langcode, $cache, $cache_id) {
  $key = md5($text);
  $cache = cache_get($key, "latexml_cache");
  if ($cache != FALSE) {
  	return $cache;
  }
  $hasPreamble = '/^([\s\S]*)\\\\begin{document}([\s\S]*)\\\\end{document}([\s\S]*)$/';
  $m = preg_match($hasPreamble, $text, $matches);
  
  // so it receives the right data
  if ($m > 0) {
  	$preamble = $matches[1];
  	$text = $matches[2];	
  } else
    $preamble = $filter->settings['latexml_preamble'];

  $host = $filter->settings['latexml_url'];
  $res = process_latexml($host, $text, $filter->settings['latexml_profile'], $preamble);

  cache_set($key, $res, "latexml_cache");

  return $res;
}